# Installation

    git clone ssh://git@gitlab.com/megor/dotvim.git ~/.vim

or

    git clone --recurse-submodules ssh://git@gitlab.com/megor/dotvim.git ~/.vim

# Create symlinks

    ln -s ~/.vim/vimrc ~/.vimrc

# Initialize submodules

    cd ~/.vim
    git submodule update --init

# Extra installation steps

Some plugins require extra setup.

## Airline

In order to take full advantage of the Airline plugin, one needs to install
Powerline fonts:

    cd ~/.vim/fonts
    ./install-powerline-fonts.sh

The above command will set the fonts for the VIM GUI.  For correct terminal VIM
support, change the font for the terminal emulator. Select any font with
"Powerline" in the name.

## Help tags

In order to initialize the help page for each plugin, run the following command:

    find ~/.vim/pack -name doc -type d -exec vim -c "helptags {}" -c q \;

# Upgrade all bundled plugins

    git submodule foreach git pull origin master

# References

<http://vimcasts.org/episodes/synchronizing-plugins-with-git-submodules-and-pathogen/>
