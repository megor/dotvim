" Exit if :Abolish isn't available.
if !exists(':Abolish')
    finish
endif

Abolish verison{,s} version{}
